from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from threading import Thread, Event
import datetime, time, random, subprocess, psutil

app = Flask (__name__)
app.config['SECRET_KEY'] = 'rahasia'
app.debug = True
socketio = SocketIO(app)
thread = Thread()
thread_stop_event = Event()

class SystemInfoThread(Thread):
    def __init__(self, socketio):
        self.delay = 1
        self.socketio = socketio
        super(SystemInfoThread, self).__init__()

    def get_cpu_info(self):
        output = psutil.cpu_percent(interval=0, percpu=True)
        server_time = "%s" % datetime.datetime.now()
        self.socketio.emit('receiveMessage', {'type':'cpu', 'msg': output, 'time':server_time}, broadcast=True)

    def get_mem_info(self):
        mem = psutil.virtual_memory()
        output = mem.percent
        server_time = "%s" % datetime.datetime.now()
        self.socketio.emit('receiveMessage', {'type':'mem', 'msg': output, 'time':server_time}, broadcast=True)

    def get_swap_info(self):
        swap = psutil.swap_memory()
        output = swap.percent
        server_time = "%s" % datetime.datetime.now()
        self.socketio.emit('receiveMessage', {'type':'swap', 'msg': output, 'time':server_time}, broadcast=True)

   def get_apache_access_log(self):
        output = subprocess.check_output('tail /var/log/apache2/access.log', shell=True)
        server_time = "%s" % datetime.datetime.now()
        self.socketio.emit('receiveMessage', {'type':'apache_access_log', 'msg': output, 'time':server_time}, broadcast=True)

    def get_apache_error_log(self):
        output = subprocess.check_output('tail /var/log/apache2/error.log', shell=True)
        server_time = "%s" % datetime.datetime.now()
        self.socketio.emit('receiveMessage', {'type':'apache_error_log', 'msg': output, 'time':server_time}, broadcast=True)

    def run(self):
        while not thread_stop_event.isSet():
            self.get_cpu_info()
            self.get_mem_info()
            self.get_swap_info()
            self.get_apache_access_log()
            self.get_apache_error_log()

            time.sleep(self.delay)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('connect', namespace='/')
def on_connect():
    print('Ada yang terhubung dengan server...')

    global thread
    if not thread.isAlive():
        print("Starting Thread")
        thread = SystemInfoThread(socketio)
        thread.start()

@socketio.on('disconnect', namespace='/')
def on_disconnect():
    print('Ada yang memutus hubungan dengan server...')

if __name__ == '__main__':
    socketio.run(app, '0.0.0.0', 5000)
